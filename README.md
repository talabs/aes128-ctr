# aes128-ctr mode encryption  

Uses test vectors from F.5.1  
https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38a.pdf  

dependencies:  
https://pub.dev/packages/encrypt  
https://pub.dev/packages/hex  


add to pubspec.yaml  
encrypt: ^3.3.1  
hex: ^0.1.2  

