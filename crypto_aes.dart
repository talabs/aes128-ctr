import 'dart:developer';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart';
import 'package:hex/hex.dart';

// TEST VECTORS section F.5.1
// https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38a.pdf

var _key = Key.fromBase16("2b7e151628aed2a6abf7158809cf4f3c");
var _iv = IV.fromBase16("f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff");
final plainText = HEX.decode("6bc1bee22e409f96e93d7e117393172a");
final testVectorEncrypted = "874d6191b620e3261bef6864990db6ce";

void aesInit() {
  try {
    log("AES128-CTR");
    log("key: " + _key.base16);
    log("iv: " + _iv.base16);
    log("plaintext: " + HEX.encode(plainText));
    final encrypted = encrypt(plainText);
    log("encrypted: " + encrypted.base16);
    final decrypted = decrypt(encrypted.bytes);
    log("decrypted: " + HEX.encode(decrypted));
    if (encrypted.base16 == testVectorEncrypted &&
        HEX.encode(plainText) == HEX.encode(decrypted)) {
      log("ENCRYPTION TESTS PASSED!");
    } else {
      log("ENCRYPTION TESTS FAILED!");
    }
  } catch (e) {
    log(e.toString());
  }
}

List<int> decrypt(Uint8List data) {
  return Encrypter(AES(_key, mode: AESMode.ctr, padding: null))
      .decryptBytes(Encrypted(data), iv: _iv);
}

Encrypted encrypt(List<int> data) {
  return Encrypter(AES(_key, mode: AESMode.ctr, padding: null))
      .encryptBytes(data, iv: _iv);
}

void setAesKey(Uint8List key) {
  _key = Key.fromBase16(HEX.encode(key.getRange(0, 16).toList()));
  log("key: " + _key.base16);
}

void setAesNonce(Uint8List nonce) {
  _iv = IV(nonce);
}
